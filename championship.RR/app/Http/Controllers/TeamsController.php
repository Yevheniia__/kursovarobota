<?php

namespace App\Http\Controllers;

use App\Models\Teams;
use Illuminate\Http\Request;

class TeamsController extends Controller
{
    public function index(Request $request){
        $type = $request->input('game_type', null);
        $city = $request->input('city', null);

        $model_teams = new Teams();


        $team = $model_teams->getTeams($type, $city);

        return view('app.teams.list', [
                'teams' => $team,
                'type' => Teams::$teams_type,
                'type_selected' => $type,
                'city_selected' => $city,]

        );
    }

    public function team($id){
        $model_teams = new Teams();
        $team = $model_teams->getTeamByID($id);
        return view('app.teams.team')->with('team',  $team);
    }
}
