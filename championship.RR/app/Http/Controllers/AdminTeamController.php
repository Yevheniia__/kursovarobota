<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Teams;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class AdminTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teams = Team::get()->sortBy('title');
        return view('admin.team.list', ['teams' => $teams]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.team.edd', ['game_type' => Teams::$teams_type]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = $request->input('title');
        $points = $request->input('points');
        $rewards = $request->input('rewards');
        $city = $request->input('city');
        $game_type = $request->input('game_type');
        $team = new Team();
        $team->points = $points ;
        $team->title = $title;
        $team->rewards = $rewards;
        $team->city = $city;
        $team->game_type = $game_type;
        $team->save();
        return Redirect::to('/admin/teams');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teams = Team::where('team_id', $id)->first();
        return view('admin.team.edit', [
            'teams' => $teams,
            'game_type' => Teams::$teams_type]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $team = Team::where('team_id', $id)->first();
        $team->title = $request->input('title');
        $team->points = $request->input('points');
        $team->rewards = $request->input('rewards');
        $team->city = $request->input('city');
        $team->game_type = $request->input('game_type') ;
        $team->save();
        return Redirect::to('/admin/teams');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Team::destroy($id);
        return Redirect::to('/admin/teams');
    }
}
