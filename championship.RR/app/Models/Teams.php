<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Teams extends Model
{
    public static $teams_type = array(
        'футбол' => 'футбол',
        'хокей' => 'хокей',
        'волейбол' => 'волейбол'
    );

    public function getCity(){
        return DB::select('select distinct(city) from championship.teams order by city');
    }

    public function getTeams($type, $city){
        $query = DB::table('teams');
        $query->select('team_id', 'title', 'points', 'rewards', 'city', 'game_type')
            ->orderBy('title');
        if($type){
            $query->where('game_type', '=', $type);
        }
        if($city){
            $query->where('city', '=', $city);
        }
        $teams = $query->get();
        return $teams;
    }

    public function getTeamByID($id){
        if(!$id) return null;
        $team = DB::table('teams')
            ->select('*')
            ->where('team_id', $id)
            ->get()->first();
        return $team;
    }
}
