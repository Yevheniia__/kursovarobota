@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
@section('content')
    <h2>Редагування групу</h2>
    <form action="/admin/teams/{{ $teams->team_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Назва команди</label>
        <input type="text" name="title" value="{{$teams->title}}">
        <br/><br/>
        <label>Кількість балів</label>
        <input type="text" name="points" value="{{$teams->points}}">
        <br/><br/>
        <label>Місце</label>
        <input type="text" name="rewards" value="{{$teams->rewards}}">
        <br/><br/>
        <label>Місто</label>
        <input type="text" name="city" value="{{$teams->city}}">
        <br/><br/>
        <label>Оберіть гру</label>
        <select name="game_type" >
            @foreach($game_type as $t => $type_title)
                <option value="{{  $t }}"
                    {{ ( $t ) ? 'selected' : '' }}>
                    {{ $type_title }}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
@endsection
