@extends('admin.layout')
@section('content')
    <h2>Список команд</h2>
    <table id="table">

        <th id="td_th">Назва команди</th>
        <th id="td_th">Кількість балів</th>
        <th id="td_th">Місце</th>
        <th id="td_th">Місто</th>
        <th id="td_th">Тип гри</th>
        <th id="td_th">Опції</th>


        @foreach ($teams as $team)
            <tr>
                <td id="td_th">
                    <a href="/teams/{{ $team->team_id }}">
                        {{ $team->title}}
                    </a>
                </td>
                <td id="td_th">{{ $team->points }}</td>
                <td id="td_th">{{ $team->rewards }}</td>
                <td id="td_th">{{ $team->city }}</td>
                <td id="td_th">{{ $team->game_type }}</td>
                    <td id="td_th"><a href="/admin/teams/{{ $team->team_id }}/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/teams/{{ $team->team_id }}"method="POST">
                        {{ method_field('DELETE') }}

                        {{ csrf_field() }}
                        <button>Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
