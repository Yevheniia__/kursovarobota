@extends('app.layouts.layout')


@section('content')
    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <p id="text">Інформація про команду: {{ $team->title }}</p>
    </div>
    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <table id="table">
            <th id="td_th">Кількість балів</th>
            <th id="td_th">Місце</th>
            <th id="td_th">Місто</th>
            <th id="td_th">Тип гри</th>
            <tr>
                <td id="td_th">{{ $team->points }}</td>
                <td id="td_th">{{ $team->rewards }}</td>
                <td id="td_th">{{ $team->city }}</td>
                <td id="td_th">{{ $team->game_type }}</td>
            </tr>
        </table>
    </div>
    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <button onclick="window.location.href = href='/teams'" id="table">Повернутись назад</button>
    </div>
@endsection
