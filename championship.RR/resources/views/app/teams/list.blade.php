@extends('app.layouts.layout')

<?php
$model_teams = new App\Models\Teams();
$c = $model_teams->getCity();

?>

@section('content')

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <form method="get" action="/teams">
            <span id="text">опції :</span>
            <select name="game_type" id="text">
                <option value="0">всі</option>
                @foreach($type as $t => $type_title)
                    <option value="{{  $t }}"
                        {{ ( $t == $type_selected ) ? 'selected' : '' }}>
                        {{ $type_title }}
                    </option>
                @endforeach
            </select>

            <select name="city" id="text">
                <option value="0">всі</option>
                @foreach($c as $city)
                    <option value="{{ $city->city }}"
                        {{ ( $city->city == $city_selected ) ? 'selected' : '' }}>
                        {{ $city->city }}
                    </option>
                @endforeach
            </select>
            <div class="container" style="display: flex; justify-content: center; align-items: center">
                <a href='/admin/teams' id="text">pедагувати</a>
            </div>
            <div class="container"  style="display: flex; justify-content: center; align-items: center">
            <p><input type="submit" id="table" value="виконати"/></p>
            </div>
            <div class="container">
                <p id="text">Список команд: </p>
            </div>
        </form>
    </div>

    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <table id="table">

            <th id="td_th">Назва команди</th>
            <th id="td_th">Місце</th>
            <th id="td_th">Місто</th>
            <th id="td_th">Тип гри</th>

            @foreach ($teams as $team)
                <tr>
                    <td id="td_th">
                        <a href="/teams/{{ $team->team_id }}">
                            {{ $team->title}}
                        </a>
                    </td>
                    <td id="td_th">{{ $team->rewards }}</td>
                    <td id="td_th">{{ $team->city }}</td>
                    <td id="td_th">{{ $team->game_type }}</td>
                </tr>
            @endforeach
        </table>
    </div>


@endsection
