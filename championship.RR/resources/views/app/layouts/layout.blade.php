<>
<head>
    <title id="text" style="display: flex; justify-content: center; align-items: center">Інформаційна система "Чемпіонат"</title>
</head>
<style>
    #table {
        font-family: Helvetica;
        font-size: 15px;
        font-weight: bold;
        width: 120px;
        text-align: center;
        border: 2px solid black;
        margin: 10px;
    }
    #td_th {
        font-family: Helvetica;
        font-size: 15px;
        font-weight: bold;
        padding-left: 35px;
        padding-right: 35px;
        height: 50px;
        text-align: center;
        border: 2px solid black;
    }
    #text {
        font-family: Helvetica;
        font-size: 17px;
        font-weight: bold;
        margin: 20px;
    }
</style>
<body>
@yield('page_title')
<br/><br/>
<div class="container" >
    <div class="container" id="text" style="display: flex; justify-content: center; align-items: center">Інформація
    </div>
    @yield('content')
</div>
<br/>
<br/>
</body>
@include('app.layouts.footer')
</html>
