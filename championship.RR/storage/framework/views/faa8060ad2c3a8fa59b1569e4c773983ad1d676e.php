<?php $__env->startSection('content'); ?>
    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <p id="text">Інформація про команду: <?php echo e($team->title); ?></p>
    </div>
    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <table id="table">
            <th id="td_th">Кількість балів</th>
            <th id="td_th">Місце</th>
            <th id="td_th">Місто</th>
            <th id="td_th">Тип гри</th>
            <tr>
                <td id="td_th"><?php echo e($team->points); ?></td>
                <td id="td_th"><?php echo e($team->rewards); ?></td>
                <td id="td_th"><?php echo e($team->city); ?></td>
                <td id="td_th"><?php echo e($team->game_type); ?></td>
            </tr>
        </table>
    </div>
    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <button onclick="window.location.href = href='/teams'" id="table">Повернутись назад</button>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH W:\domains\championship.RR\resources\views/app/teams/team.blade.php ENDPATH**/ ?>