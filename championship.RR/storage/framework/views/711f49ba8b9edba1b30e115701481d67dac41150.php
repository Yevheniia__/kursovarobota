<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
<?php $__env->startSection('content'); ?>
    <h2>Редагування групу</h2>
    <form action="/admin/teams/<?php echo e($teams->team_id); ?>" method="POST">
        <?php echo e(method_field('PUT')); ?>

        <?php echo e(csrf_field()); ?>

        <label>Назва команди</label>
        <input type="text" name="title" value="<?php echo e($teams->title); ?>">
        <br/><br/>
        <label>Кількість балів</label>
        <input type="text" name="points" value="<?php echo e($teams->points); ?>">
        <br/><br/>
        <label>Місце</label>
        <input type="text" name="rewards" value="<?php echo e($teams->rewards); ?>">
        <br/><br/>
        <label>Місто</label>
        <input type="text" name="city" value="<?php echo e($teams->city); ?>">
        <br/><br/>
        <label>Оберіть гру</label>
        <select name="game_type" >
            <?php $__currentLoopData = $game_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $t => $type_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($t); ?>"
                    <?php echo e(( $t ) ? 'selected' : ''); ?>>
                    <?php echo e($type_title); ?>

                </option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH W:\domains\championship.RR\resources\views/admin/team/edit.blade.php ENDPATH**/ ?>