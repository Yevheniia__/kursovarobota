<?php
$model_teams = new App\Models\Teams();
$c = $model_teams->getCity();

?>

<?php $__env->startSection('content'); ?>

    <div class="container"  style="display: flex; justify-content: center; align-items: center">
        <form method="get" action="/teams">
            <span id="text">опції :</span>
            <select name="game_type" id="text">
                <option value="0">всі</option>
                <?php $__currentLoopData = $type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $t => $type_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($t); ?>"
                        <?php echo e(( $t == $type_selected ) ? 'selected' : ''); ?>>
                        <?php echo e($type_title); ?>

                    </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>

            <select name="city" id="text">
                <option value="0">всі</option>
                <?php $__currentLoopData = $c; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $city): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <option value="<?php echo e($city->city); ?>"
                        <?php echo e(( $city->city == $city_selected ) ? 'selected' : ''); ?>>
                        <?php echo e($city->city); ?>

                    </option>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </select>
            <div class="container" style="display: flex; justify-content: center; align-items: center">
                <a href='/admin/teams' id="text">pедагувати</a>
            </div>
            <div class="container"  style="display: flex; justify-content: center; align-items: center">
            <p><input type="submit" id="table" value="виконати"/></p>
            </div>
            <div class="container">
                <p id="text">Список команд: </p>
            </div>
        </form>
    </div>

    <div class="container" style="display: flex; justify-content: center; align-items: center">
        <table id="table">

            <th id="td_th">Назва команди</th>
            <th id="td_th">Місце</th>
            <th id="td_th">Місто</th>
            <th id="td_th">Тип гри</th>

            <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td id="td_th">
                        <a href="/teams/<?php echo e($team->team_id); ?>">
                            <?php echo e($team->title); ?>

                        </a>
                    </td>
                    <td id="td_th"><?php echo e($team->rewards); ?></td>
                    <td id="td_th"><?php echo e($team->city); ?></td>
                    <td id="td_th"><?php echo e($team->game_type); ?></td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </table>
    </div>


<?php $__env->stopSection(); ?>

<?php echo $__env->make('app.layouts.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH W:\domains\championship.RR\resources\views/app/teams/list.blade.php ENDPATH**/ ?>