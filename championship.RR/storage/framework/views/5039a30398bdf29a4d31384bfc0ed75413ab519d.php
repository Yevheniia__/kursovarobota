<?php $__env->startSection('content'); ?>
    <h2>Список команд</h2>
    <table id="table">

        <th id="td_th">Назва команди</th>
        <th id="td_th">Кількість балів</th>
        <th id="td_th">Місце</th>
        <th id="td_th">Місто</th>
        <th id="td_th">Тип гри</th>
        <th id="td_th">Опції</th>


        <?php $__currentLoopData = $teams; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $team): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td id="td_th">
                    <a href="/teams/<?php echo e($team->team_id); ?>">
                        <?php echo e($team->title); ?>

                    </a>
                </td>
                <td id="td_th"><?php echo e($team->points); ?></td>
                <td id="td_th"><?php echo e($team->rewards); ?></td>
                <td id="td_th"><?php echo e($team->city); ?></td>
                <td id="td_th"><?php echo e($team->game_type); ?></td>
                    <td id="td_th"><a href="/admin/teams/<?php echo e($team->team_id); ?>/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/teams/<?php echo e($team->team_id); ?>"method="POST">
                        <?php echo e(method_field('DELETE')); ?>


                        <?php echo e(csrf_field()); ?>

                        <button>Delete</button>
                    </form>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </table>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH W:\domains\championship.RR\resources\views/admin/team/list.blade.php ENDPATH**/ ?>