</head>
<style>
    #table {
        font-family: Helvetica;
        font-size: 15px;
        font-weight: bold;
        width: 120px;
        text-align: center;
        border: 2px solid black;
        margin: 10px;
    }
    #td_th {
        font-family: Helvetica;
        font-size: 15px;
        font-weight: bold;
        padding-left: 35px;
        padding-right: 35px;
        height: 50px;
        text-align: center;
        border: 2px solid black;
    }
    #text {
        font-family: Helvetica;
        font-size: 17px;
        font-weight: bold;
        margin: 20px;
    }
</style>
<div class="container"  style="display: flex; justify-content: center; align-items: center">
<h1 id="text">Адмінка</h1>
</div>
<div class="leftnav" style="float: left; width: 150px; height: 100%; margin-right:
30px;">
    <b id="text">Команди</b>
    <ul>
        <li><a href="/admin/teams" id="text">список</a></li>
        <li><a href="/admin/teams/create" id="text">додати</a></li>
    </ul>
    <br/>
    <ul>
        <li><a href="/teams" id="text">Повернутися до головної сторінки</a></li>
    </ul>
</div>
<div>
    <?php echo $__env->yieldContent('content'); ?>
</div>
<?php /**PATH W:\domains\championship.RR\resources\views/admin/layout.blade.php ENDPATH**/ ?>