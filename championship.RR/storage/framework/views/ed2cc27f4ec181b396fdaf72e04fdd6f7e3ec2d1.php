<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
</style>
<?php $__env->startSection('content'); ?>
    <h2>Редагування групи</h2>
    <form action="/admin/teams" method="POST">
        <?php echo e(csrf_field()); ?>

        <label>Назва команди</label>
        <input type="text" name="title">
        <br/><br/>
        <label>Кількість балів</label>
        <input type="text" name="points">
        <br/><br/>
        <label>Місце</label>
        <input type="text" name="rewards">
        <br/><br/>
        <label>Місто</label>
        <input type="text" name="city">
        <br/><br/>
        <label>Тип гри</label>
        <select name="game_type" >
            <?php $__currentLoopData = $game_type; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $t => $type_title): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <option value="<?php echo e($t); ?>"
                    <?php echo e(( $t ) ? 'selected' : ''); ?>>
                    <?php echo e($type_title); ?>

                </option>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </select>
        <br/>
        <br/>
        <input type="submit" value="Зберегти">
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layout', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH W:\domains\championship.RR\resources\views/admin/team/edd.blade.php ENDPATH**/ ?>