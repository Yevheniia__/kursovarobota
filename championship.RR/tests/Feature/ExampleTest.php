<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\Event;
use App\Models\Team;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    /** @test  */
    public function TestPage()
    {
        $response = $this->get('http://tt/admin/teams')
            ->assertOk();
    }

    /** @test  */

    public function TestPost(){
        Event::fake();

        $response = $this->post('http://tt/admin/teams', [
            'title' => 'Динамо',
            'points' => '22',
            'rewards' => '1',
            'city' => 'Київ',
            'game_type' => 'футбол'

        ]);

        $this->assertCount(8, Team::all());


    }
}
