<?php

class TeamsCest
{
    public function _before(FunctionalTester $I)
    {
    }

    // tests
    public function tryToTest(FunctionalTester $I)
    {
    }

    public function testMainPage(FunctionalTester $I)
    {
        $I->amOnPage('http://tt/teams');
        $I->canSee('Список команд:', 'p');
    }

    public function testGetTeamByCity(FunctionalTester $I)
    {
        $I->haveInDatabase('teams',
            array('team_id' => '1',
                'city' => 'Батєво',
                'title' => 'Бригада',
                'rewards' => '1',
                'game_type' => 'хокей',
                'points' => '21'
            ));
        //'team_id', 'title', 'points', 'rewards', 'city', 'game_type'

        $I->seeInDatabase('teams', ['city' => 'Батєво']);
    }


    //vendor\bin\codecept run --steps
    //vendor\bin\codecept run --steps -html
}
